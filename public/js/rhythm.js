Rhythm = {
    init : function() {
        try {
            // vendor prefixy
            window.AudioContext = window.AudioContext||window.webkitAudioContext;
            APP.first_context = new AudioContext();
            APP.second_context = new AudioContext();
        } catch(e) {
            alert('Web Audio API is not supported in this browser');
        }

        APP.first_clock = new WAAClock(APP.first_context, {toleranceEarly: 0.1});
        APP.second_clock = new WAAClock(APP.second_context, {toleranceEarly: 0.1});
        // casy jsou ve vterinach - hodnoty jsou v BPM
        Rhythm.First = {};
        Rhythm.Second = {};
        Rhythm.First.beat_len = 60/APP.first_tempo;
        Rhythm.Second.beat_len = 60/APP.second_tempo;

        Rhythm.First.bar_len = APP.first_signature * Rhythm.First.beat_len;
        Rhythm.Second.bar_len = APP.second_signature * Rhythm.Second.beat_len;
    },
    // vypocet absolutniho casu dalsiho beatu
    // rhythm index je first nebo second!
    next_beat_time : function(beat_index, rhythm_index) {
        var currentTime = APP[rhythm_index + '_context'].currentTime;
        // nechce se mi s tim moc jebat
        // ale mam tam ruznou velikost pismena u indexu
        // tak si ho zvetsime
        rhythm_index = rhythm_index.charAt(0).toUpperCase() + rhythm_index.slice(1);

        var currentBar = Math.floor(currentTime / Rhythm[rhythm_index].bar_len);
        var currentBeat = Math.round(currentTime % Rhythm[rhythm_index].bar_len);

        if (currentBeat < beat_index) {
            return currentBar * Rhythm[rhythm_index].bar_len + beat_index * Rhythm[rhythm_index].beat_len;
        } else {
            return (currentBar + 1) * Rhythm[rhythm_index].bar_len + beat_index * Rhythm[rhythm_index].beat_len;
        }
    }
};
