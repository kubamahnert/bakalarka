Guide = {
    attemptToShow : function() {
        if (! $.cookie('seen-guide')) {
            // nevideli, zobrazime
            Guide.show();
            $.cookie('seen-guide', 'seen');
        }
    },
    show : function() {
        // ukazeme
        $('#guide').fadeIn();
        // navesime
        $('#guide #continue').click(function() {
            $('#guide').fadeOut();
        });
    }
}
