Instrument = {
    strobe : function(classname) {
        Instrument.turn_on('.strobe' + '.' + classname);
        setTimeout(function() {
            Instrument.turn_off('.strobe' + '.' + classname);
        }, 300);
    },
    red_led : function(classname) {
        Instrument.turn_on('.red-led' + '.' + classname);
        setTimeout(function() {
            Instrument.turn_off('.red-led' + '.' + classname);
        }, 100);
    },
    green_led : function(classname) {
        Instrument.turn_on('.green-led' + '.' + classname);
        setTimeout(function() {
            Instrument.turn_off('.green-led' + '.' + classname);
        }, 120);
    },
    wave : function(classname) {
        Instrument.turn_on('.wave' + '.' + classname);
        setTimeout(function() {
            Instrument.turn_off('.wave' + '.' + classname);
        }, 200);
    },
    metronome : function() {
        Instrument.turn_on('#metronome');
        setTimeout(function() {
            Instrument.turn_off('#metronome');
        }, 150);
    },
    // helpery
    turn_on : function(selector) {
        $(selector).removeClass('off');
        $(selector).addClass('on');
    },
    turn_off : function(selector) {
        $(selector).removeClass('on');
        $(selector).addClass('off');
    },
    reset : function() {
        $('#strobe').addClass('off');
        $('#strobe').removeClass('on');

        $('#red-led').addClass('off');
        $('#red-led').removeClass('on');

        $('#green-led').addClass('off');
        $('#green-led').removeClass('on');

        $('#wave').addClass('off');
        $('#wave').removeClass('on');
    }
};
