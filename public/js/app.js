APP = {
    init : function(first, second) {
        APP.first_beat = 0;
        APP.second_beat = 0;

        Instrument.reset();
        APP.First = {};
        APP.Second = {};

        APP.First = first.rhythms;
        APP.Second = second.rhythms;

        APP.first_signature = first.signature;
        APP.second_signature = second.signature;

        APP.first_tempo = first.tempo;
        APP.second_tempo = second.tempo;

        Listeners.init();

        Rhythm.init();
        APP.start();
        Guide.attemptToShow();
    },
    start : function() {
        APP.first_clock.start();
        APP.second_clock.start();
        APP.first_timer = APP.first_clock.callbackAtTime(APP.first_beat_handler, Rhythm.next_beat_time(0, 'first')).repeat(Rhythm.First.beat_len);
        APP.second_timer = APP.second_clock.callbackAtTime(APP.second_beat_handler, Rhythm.next_beat_time(0, 'second')).repeat(Rhythm.Second.beat_len);
    },
    first_beat_handler : function() {
        // jestli je to prvni, i.e. nacetlo se audioAPI, tak dej vedet
        if (! APP.already_started) {
            APP.already_started = true;
            $('#splash').removeClass('active');
            $('.floating-text span').fadeIn();
        }

        if (APP.first_beat == APP.first_signature) {
            APP.first_beat = 1;
        } else {
            APP.first_beat++;
        }

        $.each(APP.First, function(instrument, rhythms) {
            if (rhythms[APP.first_beat]) {
                Instrument[instrument]('first');
            }
            // metronom jen u prvniho beatu
            Instrument.metronome();
        });
    },
    second_beat_handler : function() {
        if (APP.second_beat == APP.second_signature) {
            APP.second_beat = 1;
        } else {
            APP.second_beat++;
        }

        $.each(APP.Second, function(instrument, rhythms) {
            if (rhythms[APP.second_beat]) {
                Instrument[instrument]('second');
            }
        });
    },
};
