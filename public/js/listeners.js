Listeners = {
  init : function() {
      $('#stop').click(function() {
          APP.first_clock.stop();
          APP.second_clock.stop();
          this.click(function() {
              APP.init();
          })
      });

      $('.rating').click(function() {
          var data = {
              'id' : $(this).data('id'),
              'opponent' : $(this).data('opponent')
          };
          // odeslani ratingu
          $.post('/rating', data, function() {
              $('#splash').addClass('active');
              $('#splash h1').fadeIn();
              $('#loading-text').addClass('hidden');
              // schovame pokyny
              $('.floating-text span').fadeOut();
              // ukazeme splash a za tri vteriny reloadneme stranku
              var timeout = setTimeout(function() {
                  location.reload();
              }, 2000);
          });
      });

      $('.rating').hover(function() {
          $('.rating').addClass('overlayed');
          $(this).addClass('hovered');
          $(this).removeClass('overlayed');
      }, function() {
          $('.rating').removeClass('overlayed');
          $('.rating').removeClass('hovered');
      });
  }
};
