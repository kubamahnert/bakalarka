# Copyright 2016 Jakub Mahnert
# This file is part of Bakalarka.
# Bakalarka is free software: you can redistribute it and/or modify it under the terms of the GNU General
# Public License as published by the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
# Bakalarka is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
# You should have received a copy of the GNU General Public License along with Bakalarka. If not, see http://www.gnu.org/licenses/.


class CoreBank
    # dela nejaky ABAB
    def first
        return Proc.new do |config, rhythm_bank|
            # to je zatim dost crude
            possibilities = rhythm_bank.filter(config)
            a_pattern = possibilities.sample
            if (! a_pattern)
                raise "could not create rhythmic sequence for 'first'!"
            end
            # kdyz je jen jedna varianta, tak udelame AAAA
            if (possibilities.one?)
                b_pattern = a_pattern
            else
                b_pattern = (possibilities - [a_pattern]).sample
            end

            # posledni zachrana
            if (! b_pattern)
                b_pattern = a_pattern
            end

            # delame neco jako ABAB
            [a_pattern, b_pattern, a_pattern, b_pattern]
        end
    end

    # lepsi ABAB
    # zkousi zvetsovat on koeficient na zaklade heating pro B rytmy
    def heater
        return Proc.new do |config, rhythm_bank|
            # to je zatim dost crude
            possibilities = rhythm_bank.filter(config)
            a_pattern = possibilities.sample
            if (! a_pattern)
                raise "could not create rhythmic sequence for 'heater'!"
            end

            # aby to bylo zajimavejsi
            if (config['heating'] && config['heating']['coef'] > 0.5)
                # spatne se chladi - jednou za cas vypadne
                b_pattern = rhythm_bank.get_blank
            elsif (config['heating'] && config['heating']['coef'] < - 0.5)
                # dobre se chladi - jednou za cas bude vic
                config['on']['coef'] += config['on']['coef'] / 5;
                possibilities = rhythm_bank.filter(config)
                b_pattern = possibilities.sample
            else
                b_pattern = a_pattern
            end

            # posledni zachrana
            if (! b_pattern)
                b_pattern = a_pattern
            end

            # delame neco jako ABAB
            [a_pattern, b_pattern, a_pattern, b_pattern]
        end
    end

    def missing
        return Proc.new do |config, rhythm_bank|
            # to je zatim dost crude
            possibilities = rhythm_bank.filter(config)
            a_pattern = possibilities.sample
            if (! a_pattern)
                raise "could not create rhythmic sequence for 'missing'!"
            end

            if (config['heating'])
                # trocha nejistoty
                random = rand(0.0..2.0)
                if (random > config['heating']['coef'])
                    b_pattern = rhythm_bank.get_blank
                end
            end

            if (! b_pattern)
                b_pattern = possibilities.sample
            end

            [a_pattern, b_pattern, a_pattern, b_pattern]
        end
    end

    # dela nejjednodussi AAAA
    def simplest
        return Proc.new do |config, rhythm_bank|
            possibilities = rhythm_bank.filter(config)
            pattern = possibilities.sample
            if (! pattern)
                raise "could not create rhythmic sequence for 'simplest'!"
            end

            [pattern, pattern, pattern, pattern]
        end
    end

    # mutuje - pro kazdy dalsi bar zvetsuje koeficienty
    def mutating
        return Proc.new do |config, rhythm_bank|
            possibilities = rhythm_bank.filter(config)
            result = []
            rhythm = possibilities.sample
            if (! rhythm)
                raise "could not create rhythmic sequence for 'mutating'!"
            end
            result.push(rhythm)
            previous_rhythm = rhythm
            # zkusime zmutovat hodnoty koeficientu v konfigu
            mutated_config = config.clone
            # zbyvaji nam na doplneni tri rytmy
            (1..3).each do |i|
                mutated_config.each do |key, item|
                    if (! item)
                        next
                    else
                        # vsechny hodnoty trosku zvetsime
                        item['coef'] += item['coef'] * 0.2
                        mutated_config[key] = item
                    end
                end

                # po upraveni zkusime nasamplovat
                possibilites = rhythm_bank.filter(mutated_config)
                rhythm = possibilites.sample
                # ulozime si zalohu - posledni, na kterej jsme se trefili - a kdyz nam zmena nevyjde, tak ji bereme
                if (! rhythm)
                    rhythm = rhythm_bank.get_blank
                end

                result.push(rhythm)
            end

            result
        end
    end

    # mutuje, ale opacnym smerem a mam problem s vymyslenim nazvu - pro kazdy dalsi bar snizuje koeficienty
    def demutating
        return Proc.new do |config, rhythm_bank|
            possibilities = rhythm_bank.filter(config)
            result = []
            rhythm = possibilities.sample
            if (! rhythm)
                raise "could not create rhythmic sequence for 'demutating'!"
            end
            result.push(rhythm)
            # zkusime zmutovat hodnoty koeficientu v konfigu
            mutated_config = config.clone
            # zbyvaji nam na doplneni tri rytmy
            (1..3).each do |i|
                mutated_config.each do |key, item|
                    if (! item)
                        next
                    else
                        # vsechny hodnoty trosku zvetsime
                        item['coef'] += item['coef'] * 0.2
                        mutated_config[key] = item
                    end
                end

                # po upraveni zkusime nasamplovat
                possibilites = rhythm_bank.filter(mutated_config)
                rhythm = possibilites.sample
                if (! rhythm)
                    rhythm = rhythm_bank.get_blank
                end

                result.push(rhythm)
            end

            result
        end
    end
end
