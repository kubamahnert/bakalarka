class RhythmBank
    @@threshold = 0.2
    attr_reader :per_bar

    def initialize(tempo)
        # na kolik dilku delime kazdy bar
        @per_bar = 4
        @bank = Bank.new(tempo, @per_bar)
        @tempo = tempo
    end

    def filter(criteria)
        result = @bank.all.select do |rhythm|
            fits = true
            evaluated = evaluate(rhythm)
            criteria.each do |key, value|
                # nulova podminka znamena, ze nas to nezajima
                if (value === nil)
                    next
                # kdyz jsme takovou vlastnost nevypocitali, tak nas nezajima
                elsif (! evaluated[key])
                    next
                else
                    fits = evaluate_fitness(evaluated[key], value)
                end
            end
            fits
        end
    end

    def get_evaluations
        result = @bank.all.each do |rhythm|
            evaluate(rhythm)
        end
        return result
    end

    def get_blank
        return @bank.get_blank
    end

    def make_rhythm(data)
        return @bank.make_rhythm(data)
    end

    protected
    def evaluate(rhythm)
        rhythm.on_coef = eval_on_coef(rhythm) # kolik % casu je zarizeni zapnuto
        rhythm.front_coef = eval_front_coef(rhythm) # pomer prepnuti v prvni a druhe polovine
        rhythm.pause_coef = eval_pause_coef(rhythm) # jaka je nejdelsi mezera mezi udery?

        return {
            'on' => rhythm.on_coef,
            'front' => rhythm.front_coef,
            'pause' => rhythm.pause_coef
        }
    end

    # prevadi magicky klice v konfigu na vyrazy
    def evaluate_fitness(evaluated, config)
        case config['operator']
        when '='
            return (evaluated - config['coef']).abs >= @@threshold
        when '>'
            return evaluated >= config['coef']
        when '<'
            return evaluated <= config['coef']
        else
            raise "config file looks wrong bruh"
        end
    end

    # hodnota bude vyssi, cim vic jsou udery bliz beatu (prvni dobe)
    def eval_front_coef(rhythm)
        flattened = rhythm.flatten
        # stred musi byt lichy
        middle = (flattened.keys.max + 1)/2.0
        # mame tri intervaly - 1..x x..y a y..N
        x = middle.floor
        y = middle.ceil
        step = 0.5
        result = 0.0
        flattened.each do |key, val|
            if (val === true)
                case key
                # kvuli pripadum, kdy neco spada zaroven do 1..x i x..y
                when x..y
                    result -= step
                when 1..x
                    result += step
                when y..flattened.keys.max
                    result += step
                end
            end
        end
        return result
    end

    # kolik % casu je zarizeni zapnuto
    def eval_on_coef(rhythm)
        sum = 0
        step = 1
        flattened = rhythm.flatten
        if (flattened.keys.max != rhythm.length)
            step = step / @per_bar
        end
        flattened.each do |key, val|
            if (val == true)
                sum += 1
            end
        end

        return sum.to_f / rhythm.length
    end

    # jaka je nejdelsi mezera mezi dvema udery
    def eval_pause_coef(rhythm)
        max_len = 0
        curr_len = 0
        flattened = rhythm.flatten
        # bylo jednodussi si udelat kopii, mozna uz neni po refaktoringu potreba (volani flatten)
        cloned = rhythm.clone
        cloned.flatten!
        data = concatenate_rhythm_data(cloned.data, cloned.clone.data)

        data.each do |key, val|
            if (val == true)
                max_len = [curr_len, max_len].max
                curr_len = 0
            else
                curr_len += 1
            end
        end

        # vysledek je pomer celkove delky k maximalni
        return max_len.to_f / flattened.keys.max
    end

    # konkatenuje dvoje rhythm data
    # souhlasim, moc sem nepatri, je to spis helper metoda pro eval_pause_coef
    # ale nemam rad zanoreny scope metod
    def concatenate_rhythm_data(data_a, data_b)
        result = {};
        iterator = 1;
        (1..(data_a.keys.max)).each do |key|
            result[iterator] = data_a[key]
            iterator += 1
        end
        (1..(data_b.keys.max)).each do |key|
            result[iterator] = data_b[key]
            iterator += 1
        end

        return result
    end
end
