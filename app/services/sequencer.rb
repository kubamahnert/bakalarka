# Copyright 2016 Jakub Mahnert
# This file is part of Bakalarka.
# Bakalarka is free software: you can redistribute it and/or modify it under the terms of the GNU General
# Public License as published by the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
# Bakalarka is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
# You should have received a copy of the GNU General Public License along with Bakalarka. If not, see http://www.gnu.org/licenses/.


class Sequencer
    @@instruments = ['strobe', 'red_led', 'green_led', 'wave']

    def initialize(core_block, core_name)
        @tempo = rand(105..120)
        @config = Configuration.new
        @rhythm_bank = RhythmBank.new(@tempo)
        @signature = 16
        @core_block = core_block
        @core_name = core_name
    end

    def get_rhythms
        results = {}
        @@instruments.each do |instrument|
            # nedelaji call i send to stejne?
            results[instrument] = concatenate @core_block.call(@config.send(instrument), @rhythm_bank)
        end

        # musime rytmy zplostit - pokud je nektera ze sekvenci slozena, tak vsechny
        wrap flatten results
    end

    def get_evaluations
        @rhythm_bank.get_evaluations
    end

    private
    # normalizuje a zplosti vsechny rytmy vsech zemi
    def flatten(instruments)
        max_tempo = @tempo
        instruments.each do |instrument, rhythm|
            normalize_tempo(rhythm)
            rhythm.flatten!
            max_tempo = [rhythm.tempo, max_tempo].max
        end

        instruments.each do |instrument, rhythm|
            if (rhythm.tempo != max_tempo)
                rhythm.widen_to_tempo(max_tempo)
            end
        end

        @signature *= max_tempo / @tempo
        @tempo = max_tempo

        return instruments
    end

    # vytvori jeden rytmus, ktery je konkatenaci vsech poslanych rytmu
    def concatenate(rhythms)
        result = {}
        index = 1
        rhythms.each do |rhythm|
            if (! rhythm)
                raise "got blank rhythm object for core #{@core_name}"
            end
            rhythm.data.each do |key, beat|
                result[index] = beat
                index += 1
            end
        end

        @rhythm_bank.make_rhythm(result)
    end

    def normalize_tempo(rhythm)
        complexity = rhythm.get_complexity
        # pokud je rytmus uvnitr deleny, je potreba tempo vynasobit tolikrat, na kolik dilku delime zakladni beat
        if (complexity > 1)
            # kdyz delim bar na dalsi ctyri bars, tak musim ctyrikrat zrychlit
            tempo = @tempo * ((complexity - 1) * @rhythm_bank.per_bar)
            signature = @signature * ((complexity - 1) * @rhythm_bank.per_bar)

            rhythm.tempo = tempo
            rhythm.signature = signature
        end

    end

    def wrap(rhythms)
        # serializujem
        rhythms.each do |instrument, rhythm|
            rhythms[instrument] = rhythm.data
        end
        {
            'rhythms' => rhythms,
            'signature' => @signature,
            'tempo' => @tempo
        }
    end
end
