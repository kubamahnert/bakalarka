# Copyright 2016 Jakub Mahnert
# This file is part of Bakalarka.
# Bakalarka is free software: you can redistribute it and/or modify it under the terms of the GNU General
# Public License as published by the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
# Bakalarka is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
# You should have received a copy of the GNU General Public License along with Bakalarka. If not, see http://www.gnu.org/licenses/.


class Configuration
    def strobe
        {
            'on' => {
                'coef' => 0.3,
                'operator' => '=',
            },
            'front' => {
                'coef' => 0.75,
                'operator' => '=',
            },
            'heating' => nil,
            'pause' => {
                'coef' => 0.6,
                'operator' => '>'
            },
        }
    end

    def red_led
        {
            'on' => {
                'coef' => 0.7,
                'operator' => '>',
            },
            'front' => nil,
            'heating' => {
                'coef' => -0.6,
                'operator' => '='
            },
            'pause' => {
                'coef' => 0.2,
                'operator' => '<'
            },
        }
    end

    def green_led
        {
            'on' => {
                'coef' => 0.6,
                'operator' => '>',
            },
            'front' => {
                'coef' => 0.9,
                'operator' => '<'
            },
            'heating' => {
                'coef' => 0.7,
                'operator' => '='
            },
            'pause' => {
                'coef' => 0.3,
                'operator' => '<'
            }
        }
    end

    def wave
        {
            'on' => {
                'coef' => 0.0,
                'operator' => '=',
            },
            'front' => {
                'coef' => 0.0,
                'operator' => '<',
            },
            'heating' => {
                'coef' => 0.6,
                'operator' => '='
            },
            'pause' => {
                'coef' => 0.8,
                'operator' => '>'
            },
        }
    end
end
