# Copyright 2016 Jakub Mahnert
# This file is part of Bakalarka.
# Bakalarka is free software: you can redistribute it and/or modify it under the terms of the GNU General
# Public License as published by the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
# Bakalarka is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
# You should have received a copy of the GNU General Public License along with Bakalarka. If not, see http://www.gnu.org/licenses/.


class TextBank
    def all
        [
            'popoháníme trpaslíky',
            "načítám aplikaci",
            "přikládáme do kotle",
            "roztáčíme kola osudu",
            "učíme ptáky létat",
            "čekáme na spojení",
            "rozdáváme opicím psací stroje",
            "polarizujeme polarizační polarizátory",
            "studujeme UML diagram",
            "katechizujeme nevěřící",
            "optimalizujeme alianci se Zorgy",
            "normalizujeme nahrávací hlášky",
            "kolorizujeme paví pera",
            "hledáme jehlu v kupce sena",
            "nosíme dříví do lesa",
            "nabíjíme tokový kondenzátor",
            "zrychlujeme na 88 m/h",
            "legitimizujeme zobrazení",
            "přijímáme data z kosmu",
            "legalizujeme nestandardní vzory",
            "synchronizujeme dilatační kyvadlo",
            "distribuujeme data do Vašeho přijímače",
        ]
    end
end
