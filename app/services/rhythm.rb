# Copyright 2016 Jakub Mahnert
# This file is part of Bakalarka.
# Bakalarka is free software: you can redistribute it and/or modify it under the terms of the GNU General
# Public License as published by the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
# Bakalarka is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
# You should have received a copy of the GNU General Public License along with Bakalarka. If not, see http://www.gnu.org/licenses/.


class Rhythm
    attr_accessor :data, :on_coef, :front_coef, :pause_coef, :tempo, :signature

    def initialize(json, tempo, per_bar)
        @data = json
        @tempo = tempo
        @per_bar = per_bar
    end

    def clone
        return Rhythm.new(@data, @tempo, @per_bar)
    end

    def length
        @data.keys.max
    end

    def flatten!
        @data = flatten_data(@data)
        return self
    end

    def flatten
        return flatten_data(@data)
    end

    def get_complexity
        eval_complexity(@data)
    end

    def widen_to_tempo(tempo)
        if (tempo == @tempo)
            return
        end

        # ensurneme zplosteni
        flatten!
        # kolikrat budeme muset delat operaci
        depth_increment = tempo / @tempo / @per_bar
        result = @data
        # depth_increment.times do
            result.each do |key, value|
                if (value == true)
                    result[key] = get_expanded_true
                else
                    result[key] = get_expanded_false
                end
            end
        # end
        @data = result
        flatten!
    end

    private
    # vraci nasobek zakladniho tempa pro normalizaci na tempo jednoduche
    def eval_complexity(rhythm_data)
        complexity = 1
        if (is_scalar(rhythm_data))
            return complexity
        end
        rhythm_data.each do |key, bar|
            if (is_scalar(bar))
                next
            else
                complexity = [complexity, eval_complexity(bar) + 1].max
            end
        end

        return complexity
    end

    def flatten_data(rhythm_data)
        depth = eval_complexity(rhythm_data)

        if (depth == 1)
            return rhythm_data
        end

        result = {}
        bar = 1
        rhythm_data.each do |key, value|
            if (is_scalar(value))
                result[bar] = value
                bar += 1
                # opadujeme - ten prvni uz mame
                for i in 1..(@per_bar - 1)
                    result[bar] = false
                    bar += 1
                end
            else
                # je to hulvat dalsi rytmus - zplostime si
                flattened = flatten_data(value)
                # nacpeme ho do mustru
                flattened.each do |k, v|
                    result[bar] = v
                    bar += 1
                end
            end
        end

        return result
    end

    # kdyz je hodnota skalar, tak je boolovska
    # jen u boolovskych hodnot plati, ze je jejich dvojita negace rovna puvodni hodnote
    def is_scalar(bar)
        return !!bar == bar
    end

    # helper vraceci pro rozsirovani rytmu na jemnejsi dilecky
    def get_expanded_true
        {
            1 => true,
            2 => false,
            3 => false,
            4 => false,
        }
    end

    def get_expanded_false
        {
            1 => false,
            2 => false,
            3 => false,
            4 => false
        }
    end
end
