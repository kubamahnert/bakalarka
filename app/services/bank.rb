# Copyright 2016 Jakub Mahnert
# This file is part of Bakalarka.
# Bakalarka is free software: you can redistribute it and/or modify it under the terms of the GNU General
# Public License as published by the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
# Bakalarka is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
# You should have received a copy of the GNU General Public License along with Bakalarka. If not, see http://www.gnu.org/licenses/.


class Bank
    def initialize(tempo, per_bar)
        @tempo = tempo
        @per_bar = per_bar
    end

    def all
        result = get_data.map do |item|
            make_rhythm(item)
        end

        return result
    end

    def make_rhythm(data)
        Rhythm.new(data, @tempo, @per_bar)
    end

    def get_blank
        make_rhythm({
            1 => false,
            2 => false,
            3 => false,
            4 => false
        })
    end

    protected
    def get_data
        [
            {
                1 => {
                    1 => true,
                    2 => false,
                    3 => true,
                    4 => false,
                },
                2 => {
                    1 => true,
                    2 => false,
                    3 => true,
                    4 => false,
                },
                3 => {
                    1 => true,
                    2 => false,
                    3 => true,
                    4 => false,
                },
                4 => {
                    1 => true,
                    2 => false,
                    3 => true,
                    4 => false,
                },
            },
            {
                1 => {
                    1 => true,
                    2 => false,
                    3 => true,
                    4 => false,
                },
                2 => false,
                3 => false,
                4 => false,
            },
            {
                1 => false,
                2 => false,
                3 => false,
                4 => {
                    1 => false,
                    2 => true,
                    3 => false,
                    4 => true
                }
            },
            {
                1 => false,
                2 => false,
                3 => false,
                4 => {
                    1 => true,
                    2 => false,
                    3 => true,
                    4 => false,
                }
            },
            {
                1 => true,
                2 => false,
                3 => true,
                4 => false
            },
            {
                1 => true,
                2 => false,
                3 => false,
                4 => false,
            },
            {
                1 => false,
                2 => false,
                3 => true,
                4 => false
            },
            {
                1 => true,
                2 => true,
                3 => true,
                4 => true
            },
            {
                1 => false,
                2 => false,
                3 => false,
                4 => true
            },
            {
                1 => false,
                2 => true,
                3 => false,
                4 => false,
            },
            {
                1 => true,
                2 => true,
                3 => false,
                4 => false
            },
            {
                1 => false,
                2 => true,
                3 => false,
                4 => true,
            },
            {
                1 => true,
                2 => true,
                3 => false,
                4 => true
            }
        ]
    end
end
