# Copyright 2016 Jakub Mahnert
# This file is part of Bakalarka.
# Bakalarka is free software: you can redistribute it and/or modify it under the terms of the GNU General
# Public License as published by the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
# Bakalarka is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
# You should have received a copy of the GNU General Public License along with Bakalarka. If not, see http://www.gnu.org/licenses/.

class IndexController < ApplicationController
  def index
  end

  def demo
      @cores = Core.all
      core_bank = CoreBank.new
      # vybereme dve jadra do tournamentu
      # prvni bude takove, ktere ma za sebou nejmene odehranych zapasu
      first_core = @cores.min_by(&:total_tournaments)
      # druhe vybereme nahodne ze zbylych
      second_core = (@cores - [first_core]).sample

      first_core_block = core_bank.send(first_core.name)
      second_core_block = core_bank.send(second_core.name)

      first_sequencer = Sequencer.new(first_core_block, first_core.name)
      second_sequencer = Sequencer.new(second_core_block, second_core.name)

      @first_rhythm = first_sequencer.get_rhythms.to_json
      @second_rhythm = second_sequencer.get_rhythms.to_json
      @first_id = first_core.id
      @second_id = second_core.id
      # sekvencery jsou stejny, je jedno, ktery vrati evaluations
      @evals = first_sequencer.get_evaluations
      @first_core_name = first_core.name
      @second_core_name = second_core.name

      @loading_text = TextBank.new.all.sample
  end

  def rating
      winner = Core.find(params[:id])
      loser = Core.find(params[:opponent])

      winner.wins += 1
      loser.losses += 1
      winner.save
      loser.save

      render :nothing => true
  end
end
